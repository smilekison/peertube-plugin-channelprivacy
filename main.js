// import RegisterServerOptions from "@peertube/peertube-types";

async function register({ peertubeHelpers }) {
  return await peertubeHelpers.database.query(
    `CREATE TABLE IF NOT EXISTS "videoChannelPrivacy" (
      "id" serial,
      "channelId" integer not null,
      "channelPrivacy" integer default 0,
      primary key ("id")
    );`,
    // console.log("This is type of file:: ", RegisterServerOptions)
    console.log("The tabel is created in database"),
  );
}

async function unregister(RegisterServerOptions) {
  return await RegisterServerOptions.peertubeHelpers.database.query(
    `DROP TABLE IF EXISTS "videoChannelPrivacy";`,
    console.log("Table is dropped")
  );
}

module.exports = {
  register,
  unregister,
};
